# Node Task

This project conatins node hello-world

## Installation steps

1. Copy the repository
2. Install dependencies
3. Start the app

## Terminal commands

*Code block:*

```sh
git clone 
cd ./node-task
npm i
```

**Start cmd:** `npm run start`

## App should work

![alt text for test image](https://baap.ponet.fi/share/mosquitto_broker.png)