const express = require('express')
const app = express()
const port = 3000

/**
 * This function adds two numbers together
 * @param {Number} a
 * @param {Number} b
 * @returns {Number}
 */
const add = (a, b) => {
    return a + b;
}

/**
 * This function shows number of person's messages
 * @param {String} name 
 * @param {Number} nbr 
 * @returns {String}
 */
const message = (name, nbr) => {
  return `${name} has ${nbr} new messages`;
}

/** Path to add function */
app.get('/add/', (req, res) => {
    const x = add(1, 2);
    res.send(`Sum: ${x}`);
})

// Path to show messages
app.get('/messages/', (req, res) => {
  const msg = message("Pena", 2);
  res.send(msg);
})

app.get('/dadd', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);

    if (isNaN(a) || isNaN(b)) {
        res.send(`params a: ${a}, b: ${b}`);
    } else {
      const sum = add(a, b);
      res.send(`Sum is: ${sum.toString()}`);
    }
})

// Welcome page
app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Listening at http://localhost:${port}`)
})